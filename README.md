# Giới thiệu
> - Tra cứu thông tin các thành phố và đất nước trên thế giới
> - Đăng nhập và Cập nhật thông tin các thành phố và đất nước trên thế giới.
> - Kiểm tra tình trạng các IP mặc định.
> Thông tin chi tiết tham khảo tại "ASP.NET Core 6 and Angular - Fifth Edition: Full-stack web development with ASP.NET 6 and Angular 13"
## Môi trường
> - Visual Studio 2022 Community
> - Microsoft.EntityFrameworkCore.Tools version 7.0.5 (dotnet tool install --global dotnet-ef)
> - nvm
> - angular/cli@13.0.1
> - MSSQL 2022
## Cài đặt
> - Clone project về máy:
>```git
>https://gitlab.com/vosonnam95/angular.git
>```
> - Với project WorldCities(Chapter 5-10),tạo DataBase mới và thay vào ConnectionStrings.DefaultConnection trong file appsettings.Development.json
> - Mở cửa sổ CMD mới và dẫn đến "./WorldCities/WorldCitiesAPI"
> - Tạo các bảng cần thiết với lệnh "dotnet ef database update" và đợi thông báo thành công.
> - Chạy chương trình
> - Để thêm dữ liệu vào các bảng, truy cập Vào url API 
>```url
>https://localhost:port/api/Seed/Import
>```
> - Với project WorldCities(Chapter 11-...), sau khi mở cửa sổ cmd và dẫn đến "./WorldCities/WorldCitiesAPI"
> - Cập nhật thêm các bảng với lệnh "dotnet ef database update"
> - Hoặc xóa các bảng hiện có và tạo lại "dotnet ef database drop" và "dotnet ef database update".
> - Làm tương tự với Chapter 5 để thêm dữ liệu vào các bảng.
> - Để thêm User mặc định và mật khẩu mặc định được đọc trong appsettings.Development.json 
>```url
>https://localhost:port/api/Seed/CreateDefaultUsers
>```
## Công nghệ sử dụng
> - **Front end:** html, css, typescript, angular.
> - **Back end:** API NET Core, C#
> - **Database:** SQL Server
