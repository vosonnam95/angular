﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System.Security;
using WorldCitiesAPI.Data.Models;
using WorldCitiesAPI.Models;
using WorldCitiesAPI.Models.Epplus;

namespace WorldCitiesAPI.Controllers
{
    [Route("data/[controller]/[action]")]
    [ApiController]
    public class SeedController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _configuration;

        public SeedController(
            ApplicationDbContext context,
            RoleManager<IdentityRole> roleManager,
            UserManager<ApplicationUser> userManager,
            IWebHostEnvironment env,
            IConfiguration configuration)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
            _env = env;
            _configuration = configuration;
        }

        [HttpGet]
        public async Task<ActionResult> Import()
        {
            if (!_env.IsDevelopment())
                throw new SecurityException("Not allowed");

            FileInfo file = new FileInfo(@"C:\Users\ASUS\source\repos\WorldCities\WorldCitiesAPI\Data\Source\worldcities.xlsx");

            using var package = new ExcelPackage(file);
            using var countriesSheet = package.Workbook.Worksheets["Countries"];

            var countries = countriesSheet.ConvertSheetToObjects<CountryDTO>().Select(i => new Country
            {
                Name = i.country,
                ISO2 = i.iso2,
                ISO3 = i.iso3
            }).ToList();
            _context.Countries.AddRange(countries);
            await _context.SaveChangesAsync();

            using var citiesSheet = package.Workbook.Worksheets["Cities"];
            var cities = citiesSheet
                .ConvertSheetToObjects<CityDTO>()
                .Join(
                    countries,
                    _city => _city.country,
                    _country => _country.Name,
                    (_city, _country) => new City()
                    {
                        Name = _city.city_ascii,
                        Lat = _city.lat,
                        Lon = _city.lng,
                        CountryId = _country.Id
                    }
                )
                .ToList();
            _context.Cities.AddRange(cities);
            await _context.SaveChangesAsync();

            return new JsonResult(new
            {
                TotalCountry = countries.Count(),
                TotalCity = cities.Count()
            });
        }

        [HttpGet]
        public async Task<ActionResult> CreateDefaultUsers()
        {
            if (!_env.IsDevelopment())
                throw new SecurityException("Not allowed");
            string[] _roles = new string[]
            {
                RoleUser.Role.Admin.ToString(),
                RoleUser.Role.Customer.ToString()
            };

            string[] _userNames = new string[]
            {
                "admin@email.com",
                "user@email.com"
            };

            string[] _userPasses = new string[]
            {
                _configuration["DefaultPasswords:Administrator"],
                _configuration["DefaultPasswords:RegisteredUser"]
            };

            foreach (string _role in _roles)
            {
                if (await _roleManager.FindByNameAsync(_role) == null)
                    _roleManager.CreateAsync(new IdentityRole(_role)).Wait();
            }

            List<ApplicationUser> _users = new List<ApplicationUser>();
            foreach (var _item in _userNames.Select((val, id) => new { UserName = val, index = id }))
            {
                if (await _userManager.FindByNameAsync(_item.UserName) == null)
                {
                    var _user = new ApplicationUser()
                    {
                        SecurityStamp = Guid.NewGuid().ToString(),
                        UserName = _item.UserName,
                        Email = _item.UserName,
                        EmailConfirmed = true,
                        LockoutEnabled = false
                    };
                    _userManager.CreateAsync(_user, _userPasses[_item.index]).Wait();
                    _userManager.AddToRoleAsync(_user, _roles[_item.index]).Wait();
                    _users.Add(_user);
                }
            }

            if (_users.Count > 0)
                await _context.SaveChangesAsync();
            return new JsonResult(new
            {
                Count = _users.Count,
                Users = _users
            });
        }

        [HttpGet]
        public async Task<bool> FailedConnect()
        {
            return await _context.Database.CanConnectAsync();
        }

    }
}
