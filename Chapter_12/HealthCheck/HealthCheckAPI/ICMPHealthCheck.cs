﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System.Net.NetworkInformation;

namespace HealthCheckAPI
{
    public class ICMPHealthCheck : IHealthCheck
    {
        private readonly string Host;
        private readonly int HealthyRoudtripTime;

        public ICMPHealthCheck(string host, int healthyRoudtripTime)
        {
            Host = host;
            HealthyRoudtripTime = healthyRoudtripTime;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(
            HealthCheckContext context, 
            CancellationToken cancellationToken = default)
        {
            try
            {
                using var ping = new Ping();
                var result = await ping.SendPingAsync(Host);
                switch (result.Status)
                {
                    case IPStatus.Success:
                        var msg =
                            $"ICMP to {Host} took {result.RoundtripTime} ms.";
                        return (result.RoundtripTime > HealthyRoudtripTime) ?
                            HealthCheckResult.Degraded(msg) :
                            HealthCheckResult.Healthy(msg);

                    default:
                        var err =
                            $"ICMP to {Host} failed: {result.Status}";
                        return HealthCheckResult.Unhealthy(err);
                }
            }catch(Exception e)
            {
                var err =
                    $"ICMP to {Host} failed: {e.Message}";
                return HealthCheckResult.Unhealthy(err);
            }
        }
    }
}
