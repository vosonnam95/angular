﻿using Microsoft.AspNetCore.Mvc;

namespace WorldCitiesAPI.Controllers
{
    public class SampleController : Controller
    {
        public ILogger<SampleController> Logger { get; set; }
        public SampleController(ILogger<SampleController> logger)
        {
            Logger = logger;
            Logger.LogInformation("SampleController initialized.");
        }
    }
}
