﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
namespace WorldCitiesAPI.Tests
{
    public static class IdentityHelper
    {
        public static RoleManager<T> GetRoleManager<T>(IRoleStore<T> roleStore)
            where T : IdentityRole
        {
            return new RoleManager<T>(
                    roleStore,
                    new IRoleValidator<T>[0],
                    new UpperInvariantLookupNormalizer(),
                    new Mock<IdentityErrorDescriber>().Object,
                    new Mock<ILogger<RoleManager<T>>>().Object
                );
        }

        public static UserManager<T> GetUserManager<T>(IUserStore<T> userStore)
            where T : IdentityUser
        {
            return new UserManager<T>(
                    userStore,
                     new Mock<IOptions<IdentityOptions>>().Object,
                     new Mock<IPasswordHasher<T>>().Object,
                     new IUserValidator<T>[0],
                     new IPasswordValidator<T>[0],
                     new UpperInvariantLookupNormalizer(),
                     new Mock<IdentityErrorDescriber>().Object,
                     new Mock<IServiceProvider>().Object,
                     new Mock<ILogger<UserManager<T>>>().Object
                );
        }
    }
}
